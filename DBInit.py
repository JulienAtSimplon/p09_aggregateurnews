""" Projet Agrégateur de news : Groupe 1 """
############################################
""" Module by Julien
- Création : 2020-03-06
- Dernière MàJ : 2020-03-18
"""

import DBProtocol, DBTab 

def TrashDB():
  DBProtocol.DropTable("AssoFluxNews")
  DBProtocol.DropTable("AssoUserFlux")
  DBProtocol.DropTable("AssoUserNews")
  DBProtocol.DropTable("TabFluxs")
  DBProtocol.DropTable("TabNews")
  DBProtocol.DropTable("TabUsers")

def InitialisationDB():
  DBProtocol.CreaTab(DBTab.TabFluxs)
  DBProtocol.CreaTab(DBTab.TabNews)
  DBProtocol.CreaTab(DBTab.TabUsers)
  DBProtocol.CreaTab(DBTab.AssoFluxNews)
  DBProtocol.CreaTab(DBTab.AssoUserFlux)
  DBProtocol.CreaTab(DBTab.AssoUserNews)


DBProtocol.ConnexionBD()
#TrashDB()
InitialisationDB()
DBProtocol.DeconnexionBD()