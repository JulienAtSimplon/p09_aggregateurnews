""" Test the function done by Alexis with pytest """

import pytest
import RSS_parse  # notre fichier rss

def test_feedparse():
    url = "https://news.ycombinator.com/rss"
    result = feedparse.recuperation_rss(url)
    news = result.get('news')
    key_news = news[0].keys()

    assert type(result) == dict
    assert 'url_flux' in result
    assert 'format' in result
    assert 'news' in result
    assert type(news) == list
    assert 'titre' in key_news
    assert 'lien_news' in key_news
    assert 'description' in key_news
    assert 'date' in key_news
    assert 'auteur' in key_news

""" If URL is not correct """
def test_bad_url():
    url = "https://news.yombinator.com/rss"
    with pytest.raises(KeyError):
        feedparse.recuperation_rss(url)
