from flask import Flask, render_template, request, redirect, url_for, make_response
from DBProtocol import ConnexionBD, ReqSQL, DeconnexionBD
from DBinsert import insert_toute_table, connexion_user,recherche, insert_utilisateur, recuperation_id, recherche_news_utilisateu
app = Flask(__name__)



@app.route('/', methods=['GET'])
def get_welcome_page():
    return render_template('welcome_page.html')

@app.route('/ajoutFlux', methods=['GET'])
def get_flow():
    return render_template('pages/flow.html')

@app.route('/diagram', methods=['GET'])
def get_diagram():
    return render_template('diagram.html')

@app.route('/home', methods=['GET'])
def get_home():
    # cur = ConnexionBD()[1]
    user_ID = request.cookies.get('id')
    pseudo = request.cookies.get('pseudo')

    # pseudo=request.args.get("pseudo")
    # sql_user="select PKId_User from TabUsers where User_name = %s "  
    # user_ID = recuperation_id(sql_user,(pseudo,))
    News = recherche_news_utilisateu(user_ID)
    # sql_news="""sELECT TabNews.*,AssoUserFlux.FK_Id_Flux  FROM TabNews
    #             JOIN AssoUserNews ON TabNews.PKId_News = AssoUserNews.FK_Id_News
    #                 WHERE AssoUserNews.FK_Id_User = %s
    #                 AND AssoUserNews.News_display = FALSE
    #             JOIN AssoUserFlux ON TabFluxs.PKId_Flux = AssoUserFlux.FK_Id_Flux
    #                 WHERE AssoUserFlux.FK_Id_User = %s
    #             AND AssoUserFlux.Flux_display = TRUE"""
    # # _,cur=ConnexionBD()
    # # cur.execute(sql_news,user_ID)
    # # News = cur.fetchall()
    # News=ReqSQL(sql_news, (user_ID,user_ID))[0]
    # cur.execute('SELECT * FROM TabFluxs;')
    # Flux = cur.fetchall()
    return render_template('pages/homepage_bootsrap.html', News=News, pseudo=pseudo)

@app.route("/login", methods=["GET","POST"])
def post_login():

    ConnexionBD()
    pseudo = request.form.get("uname")
    password = request.form.get("psw")
    sql_user="select PKId_User from TabUsers where User_name = %s "
    user_ID = recuperation_id(sql_user,(pseudo,))

    bonID, lis_user = connexion_user(pseudo, password)    
    if bonID == True:
        resp = make_response(redirect(url_for("get_home")))
        print("resp :", resp)

        # resp.set_cookie('pseudo', pseudo)
        resp.set_cookie('id', str(user_ID))
        resp.set_cookie('pseudo', pseudo)
        print("resp :", resp)
        return resp
        # return redirect(url_for("get_home"), user_ID=user_ID, pseudo=pseudo)
        # return redirect(url_for("get_home", pseudo=pseudo))
    elif [pseudo] in lis_user:
        return render_template('welcome_page.html', errorMessage="Le pseudo existe mais le mot de passe est érroné !")
    else:
        return render_template('welcome_page.html', errorMessage="Vos identifiants sont incorrects !")


@app.route("/inscription", methods=["POST"])
def post_inscription():
    ConnexionBD()
    pseudo = request.form["uname"]
    password = request.form["psw"]
    password_repeat = request.form.get("psw-repeat")

    if password_repeat == password:
        list_user = connexion_user(pseudo, password, True)
        print(list_user)
        if [pseudo] in list_user :
            return render_template('welcome_page.html', errorMessage="L'utilisateur existe déjà !")
        else:
            insert_utilisateur(pseudo, password)
            return render_template('welcome_page.html', errorMessage="Bravo vous êtes inscrit !")

    else:
        return render_template('welcome_page.html', errorMessage="Vos 2 passwords sont différents !")

@app.route("/ajoutFlux", methods=["POST"])
def post_ajoutFlux():
    _,cur=ConnexionBD()
    user_ID = request.cookies.get('id')
    pseudo = request.cookies.get('pseudo')

    nom_flux = request.form.get("title")
    url_flux = request.form.get("url")
    print("nom_flux",nom_flux)
    print("url_flux",url_flux)
    insert_toute_table(int(user_ID), nom_flux, url_flux)
    # cur.execute(sql_news,(user_ID,))
    # liste_news=cur.fetchall()
    # DeconnexionBD()
    return redirect(url_for("get_home"))


@app.route('/recherche', methods=['GET'])
def recherche_titre():

    ConnexionBD()
    recher=request.args.get("recherche")
    print(recher)
    id_user = request.cookies.get('id')

    News = recherche(recher,id_user)
    for new in News:
        print(type(new))

    return render_template("pages/homepage_bootsrap.html",News=News)
# @app.route("/", methods=["GET"])
# def post_add_source():
#     # for elem in list_NomFlux:
#     _,cur=ConnexionBD()
#      #user_id = request.cookies.get('YourSessionCookie')

#     url = request.args.get("url_flux", "") 
#     nom_flux = request.args.get("nom_flux", "") 
#     insert_toute_table(id_user,nom_flux,url)
#     DeconnexionBD()
#     # conn = db.get_db()
#     # db.add_book(conn, request.form)
#     return redirect(url_for("get_home"))

# @app.route("/", methods=["POST"])
# def post_home():
#     return "new item added"