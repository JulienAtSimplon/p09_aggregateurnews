""" Projet Agrégateur de news : Groupe 1 """
############################################
""" Module by Julien
𐤟 Création : 2020-03-06
𐤟 Dernière MàJ : 2020-03-12
"""

import psycopg2, psycopg2.extras, sys
import DBTab
################ Connexion ################

def ConnexionBD(): # /!\ Return connection & cursor as tuple
    global conn
    global cur
    #try:
        #conn
    #except NameError:
    try:
        conn = psycopg2.connect(dbname="d4rqsrptkfkrgp", user="vuopuopkqwwsop", password="6dac369e70eba4939cf0e6aa922bd62b82cd0cdf4d836832d27fd94ae1ace7de", host="ec2-54-195-247-108.eu-west-1.compute.amazonaws.com", port="5432")
    except:
        print("Connexion impossible. C'est certainement de la faute de Zahra !")
        sys.exit()
    cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    print("Connexion :",conn,"Curseur :", cur)
    return conn, cur

#conn, cur = ConnexionBD()

def DeconnexionBD():
    cur.close() 
    conn.close()

################ Générale ################

def Commit():
    conn.commit()


def ExeSQL(CodeSQL, MonTuple):
    cur.execute(CodeSQL, MonTuple)


def ReqSQL(CodeSQL, MonTuple):
    cur.execute(CodeSQL, MonTuple)
    return cur.fetchall()

################ Tables ################

def CreaTab(CodeSQL):
    try:
        cur.execute(CodeSQL)
        Commit()
        print(f"La création de table a réussi")
    except:
        print('La creation de la table a échoué')


def InsertTab(CodeSQL, dico):
    try:
        cur.execute(CodeSQL, dico)
        Commit()
        print("Ca a reussit")
    except:
        print("Ca a raté jack")

################ Manipulations des Tables ################

def DropTable(table):
    try:
        sql = f"""DROP TABLE IF EXISTS {table}"""
        cur.execute(sql)
    except:
        print("Ca a echoué")

def DropTableCasscade(table):
    try:
        sql = f"""DROP TABLE IF EXISTS {table} CASCADE"""
        cur.execute(sql)
    except:
        print("Ca a echoué")


def Del(table, condition, MonTuple):
    cur.execute(f"""DELETE FROM {table} WHERE {condition} = %s;""", MonTuple)

################ Fin ################

